import React, {Component} from "react";
import Cell from "./Cell";
import './Board.css';

// NEVER ERASE THIS JOHN !!!!
/** Game board of Lights out. 
 *
 * Properties:
 *
 * - nrows: number of rows of board
 * - ncols: number of cols of board
 * - chanceLightStartsOn: float, chance any cell is lit at start of game
 *
 * State:
 *
 * - hasWon: boolean, true when board is all off
 * - board: array-of-arrays of true/false
 *
 *    For this board:
 *       .  .  .
 *       O  O  .     (where . is off, and O is on)
 *       .  .  .
 *
 *    This would be: [[f, f, f], [t, t, f], [f, f, f]]
 *
 *  This should render an HTML table of individual <Cell /> components.
 *
 *  This doesn't handle any clicks --- clicks are on individual cells
 *
 **/

class Board extends Component {
  static defaultProps = {
    nRows: 5,
    nCols: 5,
    chanceLightStartsOn: 0.25
  };

  constructor(props) {
    super(props);
    this.state = {
      hasWon: false,
      board: this.createBoard()
    }
    // TODO: set initial state
    this.flipCellsAround = this.flipCellsAround.bind(this)
    this.resetBoard = this.resetBoard.bind(this)
  }

  /** create a board nrows high/ncols wide, each cell randomly lit or unlit */

  createBoard() {
    let board = [];
    for ( let y = 0 ; y < this.props.nRows ; y++ ) {
     let row = [] ; 
     for (let x = 0; x < this.props.nCols; x++) {
      row.push(Math.random() < this.props.chanceLightStartsOn)
     }
     board.push(row)
   }
    // create array-of-arrays of true/false values
    return board
  }

  /** handle changing a cell: update board & determine if winner */

  flipCellsAround(coord) {
    let ncols = this.props.nCols;
    let nrows = this.props.nRows;
    let board = this.state.board;
    let [y, x] = coord.split("-").map(Number);

    function flipCell(y, x) {
      // if this coord is actually on board, flip it
      if (x >= 0 && x < ncols && y >= 0 && y < nrows) {
        board[y][x] = !board[y][x] 
      }
    }

    flipCell(y,x)
    flipCell(y+1,x)
    flipCell(y-1,x)
    flipCell(y,x+1)
    flipCell(y,x-1)

    let hasWon = board.every(row => row.every(cell => cell === false))
   
    this.setState({board: board, hasWon: hasWon});


    }
    // flip this cell and the cells around it

    // win when every cell is turned off
    // determine is the game has been won
  
    resetBoard() {
      this.setState({hasWon: false});
      this.createBoard();
    }


  


  /** Render game board or winning message. */

  render() {
    if (this.state.hasWon) {
      return <div>
      <div className="Board-title">
          <div className="winner">
            <span className="neon-orange">You </span>
            <span className="neon-blue">Win !</span>
            </div>
            </div>
          <div  id="bob" className="neon-blue" onClick={this.resetBoard}>Reset ?</div>
      </div>
    }


    let theBoard = []
    this.state.board.map((r, index )=>(
       theBoard.push(<tr key={"row"+index}>
         {r.map((c, idx )=> (
           <Cell 
           isLit={c} 
           key={"row"+index+"col"+idx}
           flipCellsAroundMe={this.flipCellsAround}
           position={`${index}-${idx}`}
            />
         ))}
       </tr>)
    ))

    return(
      
       <div>
            <div className="Board-title">
            <div className="neon-orange">Lights</div>
            <div className="neon-blue">Out</div>
       </div>
       
        <table className="Board"> 
        <tbody>
          
            {theBoard}
            
        </tbody>
      </table>
     </div>
    )

    
  }
}


export default Board;
